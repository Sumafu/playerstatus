# PlayerStatus
PlayerStatus is a simple Plugin, which logs the online status of a player. The plugins saves, on which server the player was last/is online and when he switched to the server. If the player is offline, it saves, when he went offline.

## Installation ##
First, download the latest version at the left side under the menu point "Downloads", unpack the file. Copy the .jar file into the plugins folder and start the BungeeCord server. The plugin will give an error message, because it cannot login to the MySQL database. Now open the config.yml in the folder PlayerStatus in the plugins folder. Insert the access data of your MySQL database, save the file and restart your BungeeCord server. Now you should not get an error and the plugin should work.

## Commands ##
/status <player> - As result you get the information wether the player is online or not. If he is online, you get the information, no wich server is is. If he is offline , you get the information, when he disconnected on which server.
### Aliases ###
In the config.yml you can enable up to three aliases for the command: /lastseen, /seen, /online

## API ##

### How to initialize ###
First you have to add the latest build of the plugin as decency to your plugin. Next, you have to get the API class, shown in the following code:
`PlayerStatusAPI psApi = PlayerStatus.getAPI();`

You should only use the API in an extra Thread, because the API connect to an MySQL server, and if this connection is slow you will get a brief deadlock of the whole server.

### How to use ###
The API has three main methods: `UUID getUuidOfName(String)`, `String getNameOfUuid(UUID)` and `PlayerInfo getPlayerInformation(String or UUID)`.

The first two are self-describing, but the third returns an PlayerInfo object. This object includes all information, the plugins saves of the player. Here a list of the methods:

* `String getName()`

* `UUID getUUID()`

* `Timestamp getTimestamp()` returns the last server-change or disconnect

* `String getServer()`

* `Timestamp getFirstConnection()` returns the time of the first connection since the plugins was installed