package de.sumafu.PlayerStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.UUID;

class DatabaseUpdater implements Runnable {
	private Connection coc = null;
	private PlayerStatus plugin = null;
	private long delay = 10000;
	
	DatabaseUpdater(PlayerStatus plugin, Connection coc){
		this.plugin = plugin;
		this.coc = coc;
		this.delay = this.plugin.configuration.getLong("updater_delay")*1000;
	}
	
	@Override
	public void run() {
		this.plugin.getLogger().info("DatabaseUpdater started");
		
		while(!Thread.currentThread().isInterrupted()){
			try {
				Thread.sleep(this.delay);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				this.plugin.getLogger().info("DatabaseUpdater is stopping. Save latest data...");
			}
			long new_datas = this.saveData();
			plugin.getLogger().info("Saved "+new_datas+" new updates");
		}
		this.plugin.getLogger().info("DatabaseUpdater has stopped");
	}
	
	private long saveData(){
		long counter = 0;
		
		while(!this.plugin.updateQueue.isEmpty()){
			PlayerInfo currentUpdate = this.plugin.updateQueue.poll();
			if(currentUpdate==null)return counter;
			
			counter++;
			
			String name = currentUpdate.getName();
			UUID uuid = currentUpdate.getUUID();
			Timestamp time = currentUpdate.getTimestamp();
			String server = currentUpdate.getServer();
			
			try {
				Statement stmt = coc.createStatement();
				ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS `Anzahl` FROM `de.bytecraft_software.PlayerStatus.Player` WHERE `PlayerUUID` = '"+uuid.toString()+"'");
				rs.next();
				if(rs.getInt("Anzahl") == 0)
				{
					PreparedStatement ps = coc.prepareStatement("INSERT INTO `de.bytecraft_software.PlayerStatus.Player` (`PlayerUUID`, `PlayerName`, `PlayerLastConnection`, `PlayerLastServer`)"
							+ " VALUES (?, ?, ?, ?)");
					ps.setString(1, uuid.toString());
					ps.setString(2, name);
					ps.setTimestamp(3, time);
					ps.setString(4, server);
					ps.executeUpdate();
				}
				else
				{
					PreparedStatement ps = coc.prepareStatement("UPDATE `de.bytecraft_software.PlayerStatus.Player` SET `PlayerName`=?, `PlayerLastConnection`=?, `PlayerLastServer`=? WHERE `PlayerUUID`=?");
					ps.setString(1, name);
					ps.setTimestamp(2, time);
					ps.setString(3, server);
					ps.setString(4, uuid.toString());
					ps.executeUpdate();
				}
				
				
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return counter;
	}

}
