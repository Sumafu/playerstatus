package de.sumafu.PlayerStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

class DatabaseConnector {
	DatabaseConnector(PlayerStatus plugin){
		this.plugin = plugin;
	}
	private PlayerStatus plugin;
	private Connection coc = null;
	private DatabaseUpdater dtbUpd = null;
	
	boolean connectMySQL(){
		String host = plugin.configuration.getString("mysql_host");
		String database = plugin.configuration.getString("mysql_database");
		String username = plugin.configuration.getString("mysql_username");
		String password = plugin.configuration.getString("mysql_password");
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			plugin.getLogger().log(Level.SEVERE, "No JDBC driver found!");
			e.printStackTrace();
			return false;
		}
		plugin.getLogger().info("MySQL JDBC Driver Registered!");
		plugin.getLogger().info("Try to connect with database server...");
		try {
			coc = DriverManager
					.getConnection("jdbc:mysql://"+host+"/"+database, username, password);
	 
		} catch (SQLException e) {
			plugin.getLogger().log(Level.SEVERE, "Connection Failed! Check output console");
			e.printStackTrace();
			return false;
		}
		if (coc != null) {
			plugin.getLogger().info("Successfully connected to MySQL");
		} else {
			plugin.getLogger().log(Level.SEVERE, "Failed to initiate connection!");
			return false;
		}
		
		try {
			if(!this.checkTable()){
				plugin.getLogger().log(Level.SEVERE, "MySQL-database is not valid");
				return false;
			}
		} catch (SQLException e) {
			plugin.getLogger().log(Level.SEVERE, "Failed to check the database! Check output console");
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean checkTable() throws SQLException{
		Statement stmt = coc.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT COUNT(*) AS Anzahl FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'de.bytecraft_software.PlayerStatus.Player'");
		rs.next();
		if(rs.getInt("Anzahl")==0){
			String create_table_sql="CREATE TABLE IF NOT EXISTS `de.bytecraft_software.PlayerStatus.Player` ("
								+ " `PlayerID` int(11) NOT NULL,"
								+ " `PlayerUUID` text NOT NULL,"
								+ " `PlayerName` text NOT NULL,"
								+ " `PlayerFirstConnection` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
								+ " `PlayerLastConnection` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',"
								+ " `PlayerLastServer` text NOT NULL"
								+ " ) ENGINE=InnoDB DEFAULT CHARSET=latin1;";
			stmt.execute(create_table_sql);
								
			create_table_sql = " ALTER TABLE `de.bytecraft_software.PlayerStatus.Player`"
							 + " ADD PRIMARY KEY (`PlayerID`);";
			stmt.execute(create_table_sql);
			
			create_table_sql = " ALTER TABLE `de.bytecraft_software.PlayerStatus.Player`"
							 + " MODIFY `PlayerID` int(11) NOT NULL AUTO_INCREMENT;";
			stmt.execute(create_table_sql);
			plugin.getLogger().info("New MySQL table created");
			return true;
		}
		
		String get_table_colums_sql = "SHOW COLUMNS FROM `de.bytecraft_software.PlayerStatus.Player`";
		ResultSet rs2 = stmt.executeQuery(get_table_colums_sql);
		List<String> colums = new ArrayList<String>();
		while(rs2.next()){
			colums.add(rs2.getString("Field"));
		}
		if(colums.size()==6
				&& colums.contains("PlayerID")
				&& colums.contains("PlayerUUID")
				&& colums.contains("PlayerName")
				&& colums.contains("PlayerFirstConnection")
				&& colums.contains("PlayerLastConnection")
				&& colums.contains("PlayerLastServer")){
			return true;
		}
		
		return false;
	}
	
	void startUpdater(){
		this.dtbUpd = new DatabaseUpdater(plugin, coc);
		plugin.getProxy().getScheduler().runAsync(plugin, dtbUpd);
		
	}
	
	Connection getConnection(){
		return this.coc;
	}
}