package de.sumafu.PlayerStatus;

import java.util.UUID;

@SuppressWarnings("serial")
public class PlayerNeverConnectedException extends Exception {
	public PlayerNeverConnectedException(String player_name){
		super("Player '"+player_name+"' has never played on this server before");
	}
	public PlayerNeverConnectedException(UUID player_uuid){
		super("Player with UUID '"+player_uuid.toString()+"' has never played on this server before");
	}
}
