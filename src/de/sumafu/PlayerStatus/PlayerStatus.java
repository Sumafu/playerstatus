package de.sumafu.PlayerStatus;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedList;
import java.util.Queue;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.*;

public class PlayerStatus extends Plugin {
	Configuration configuration;
	boolean configReady = false;
	boolean pluginReady = false;
	
	Queue<PlayerInfo> updateQueue = new LinkedList<PlayerInfo>();
	
	private DatabaseConnector connector;
	private static PlayerStatusAPI api;
	
	@Override
    public void onEnable() {
		try{
			// Load default config if not exists
			if (!getDataFolder().exists())
	            getDataFolder().mkdir();
	
	        File file = new File(getDataFolder(), "config.yml");
	
	        if (!file.exists()) {
	            Files.copy(getResourceAsStream("config.yml"), file.toPath());
	        }
	        
			// Load Configuration
	        configuration = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
	        this.configSetDefaults();
	        
	        configReady = true;
	        
	        // Initialise database
	        connector = new DatabaseConnector(this);
	        boolean mysqlReady = connector.connectMySQL();
	        
	        if(!mysqlReady){
	        	ProxyServer.getInstance().stop();
	        	return;
	        }
	        connector.startUpdater();
	        
	        // Register Events
	        this.getProxy().getPluginManager().registerListener(this, new Events(this));
	        
	        
	        // Load API
	        api = new PlayerStatusAPI(this);
	        
	        // Register command /status
	        this.getProxy().getPluginManager().registerCommand(this, new StatusCommandExecuter(this, "status"));
	        // Register some aliases
	        if(this.configuration.getBoolean("commandAlias.lastseen"))
	        	this.getProxy().getPluginManager().registerCommand(this, new StatusCommandExecuter(this, "lastseen"));
	        
	        if(this.configuration.getBoolean("commandAlias.seen"))
	        	this.getProxy().getPluginManager().registerCommand(this, new StatusCommandExecuter(this, "seen"));
	        
	        if(this.configuration.getBoolean("commandAlias.online"))
	        	this.getProxy().getPluginManager().registerCommand(this, new StatusCommandExecuter(this, "online"));
	        
	        this.pluginReady=true;
		}catch(IOException e){
			e.printStackTrace();
		}
    }
	
	DatabaseConnector getConnector(){
		return this.connector;
	}
	public static PlayerStatusAPI getAPI(){
		return api;
	}
	
	private void configSetDefaults() throws IOException{
		if(this.configuration.get("commandAlias.lastseen")==null)this.configuration.set("commandAlias.lastseen", false);
		if(this.configuration.get("commandAlias.seen")==null)this.configuration.set("commandAlias.seen", false);
		if(this.configuration.get("commandAlias.online")==null)this.configuration.set("commandAlias.online", false);
		
		ConfigurationProvider.getProvider(YamlConfiguration.class).save(configuration, new File(getDataFolder(), "config.yml"));
	}
}
