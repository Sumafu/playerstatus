package de.sumafu.PlayerStatus;

import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class Events implements Listener {
	public Events(PlayerStatus plugin){
		this.plugin = plugin;
	}
	
	private PlayerStatus plugin;
	
	@EventHandler
	public void onServerSwitch(ServerSwitchEvent event){
		ProxiedPlayer player = event.getPlayer();
		PlayerInfo pu = new PlayerInfo(player.getName(), player.getUniqueId(), player.getServer().getInfo().getName());
		plugin.updateQueue.offer(pu);
	}
	
	@EventHandler
	public void onDisconnect(PlayerDisconnectEvent event){
		ProxiedPlayer player = event.getPlayer();
		PlayerInfo pu = new PlayerInfo(player.getName(), player.getUniqueId(), player.getServer().getInfo().getName());
		plugin.updateQueue.offer(pu);
	}
}
