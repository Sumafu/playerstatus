package de.sumafu.PlayerStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.UUID;

public class PlayerStatusAPI {
	public PlayerStatusAPI(PlayerStatus plugin){
		this.plugin = plugin;
		this.coc = this.plugin.getConnector().getConnection();
	}
	
	PlayerStatus plugin = null;
	Connection coc = null;
	
	public UUID getUuidOfName(String name) throws PlayerNeverConnectedException, SQLException{
		PreparedStatement ps = coc.prepareStatement("SELECT `PlayerUUID` FROM `de.bytecraft_software.PlayerStatus.Player` WHERE `PlayerName` = ?");
		ps.setString(1, name);
		if(ps.execute()){
			ResultSet rs = ps.getResultSet();
			if(rs.next()){
				String uuid_string = rs.getString("PlayerUUID");
				return UUID.fromString(uuid_string);
			}else{
				throw new PlayerNeverConnectedException(name);
			}
		}else{
			throw new PlayerNeverConnectedException(name);
		}
	}
	
	public String getNameOfUuid(UUID uuid) throws SQLException, PlayerNeverConnectedException{
		return this.getPlayerInformation(uuid).getName();
	}
	
	public PlayerInfo getPlayerInformation(String name) throws PlayerNeverConnectedException, SQLException{
		UUID uuid = this.getUuidOfName(name);
		return this.getPlayerInformation(uuid);
	}
	
	public PlayerInfo getPlayerInformation(UUID uuid) throws SQLException, PlayerNeverConnectedException{
		PreparedStatement ps = coc.prepareStatement("SELECT * FROM `de.bytecraft_software.PlayerStatus.Player` WHERE `PlayerUUID` = ?");
		ps.setString(1, uuid.toString());
		if(ps.execute()){
			ResultSet rs = ps.getResultSet();
			if(rs.next()){
				String name = rs.getString("PlayerName");
				Timestamp timeLast = rs.getTimestamp("PlayerLastConnection");
				Timestamp timeFirst = rs.getTimestamp("PlayerFirstConnection");
				String server = rs.getString("PlayerLastServer");
				
				PlayerInfo pi = new PlayerInfo(name, uuid, server);
				pi.setLastConnection(timeLast);
				pi.setFirstConnection(timeFirst);
				
				return pi;
			}else{
				throw new PlayerNeverConnectedException(uuid);
			}
		}else{
			throw new PlayerNeverConnectedException(uuid);
		}
	}
	
}
