package de.sumafu.PlayerStatus;

import java.sql.Timestamp;
import java.util.Date;
import java.util.UUID;

public class PlayerInfo {
	PlayerInfo(String name, UUID uuid, String server){
		this.playerName = name;
		this.playerUUID = uuid;
		this.playerLastServer = server;
		this.playerLastConnection = new Timestamp(new Date().getTime());
	}
	
	private String playerName = null;
	private UUID playerUUID = null;
	private Timestamp playerLastConnection = null;
	private String playerLastServer = null;
	
	private Timestamp playerFirstConnection = null;
	
	public String getName(){
		return this.playerName;
	}
	public UUID getUUID(){
		return this.playerUUID;
	}
	public Timestamp getTimestamp(){
		return this.playerLastConnection;
	}
	public String getServer(){
		return this.playerLastServer;
	}
	public Timestamp getFirstConnection(){
		return this.playerFirstConnection;
	}
	
	void setFirstConnection(Timestamp time){
		this.playerFirstConnection = time;
	}
	void setLastConnection(Timestamp time){
		this.playerLastConnection = time;
	}
}
