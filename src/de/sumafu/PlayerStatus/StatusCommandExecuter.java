package de.sumafu.PlayerStatus;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

class StatusCommandExecuter extends Command{
	StatusCommandExecuter(PlayerStatus plugin, String name){
		super(name);
		this.plugin = plugin;
		this.api = PlayerStatus.getAPI();
		
		this.thisCommand=name;
	}
	PlayerStatus plugin = null;
	PlayerStatusAPI api = null;
	String thisCommand;
	
	@Override
	public void execute(CommandSender cms, String[] arg1) {
		this.plugin.getProxy().getScheduler().runAsync(this.plugin, new MyRunnable(cms, arg1, this.plugin, this.thisCommand){
			@Override
			public void run() {
				CommandSender cms = (CommandSender) params[0];
				String[] arg1 = (String[]) params[1];
				PlayerStatus plugin = (PlayerStatus) params[2];
				String myCommand = (String) params[3];
				
				switch(arg1.length){
				case 0:
					TextComponent message = new TextComponent("Command use: /"+myCommand+" <player_name>");
					message.setColor(ChatColor.RED);
					cms.sendMessage(message);
					break;
				default:
					try {
						ProxiedPlayer player = plugin.getProxy().getPlayer(arg1[0]);
						if(player != null){
							String server = player.getServer().getInfo().getName();
							
							ComponentBuilder output = new ComponentBuilder(player.getDisplayName()).color(ChatColor.YELLOW);
							output.append(" is currently online at server ").color(ChatColor.GREEN);
							output.append(server).color(ChatColor.YELLOW);
							cms.sendMessage(output.create());
						}else{
							PlayerInfo playerInfo = api.getPlayerInformation(arg1[0]); 
							String name = playerInfo.getName();
							String server = playerInfo.getServer();
							Timestamp timestamp = playerInfo.getTimestamp();
							String time = new SimpleDateFormat("dd.MM.yyy").format(timestamp);
							
							ComponentBuilder output = new ComponentBuilder(name+" was last online on "+time+" at server ");
							output.append(server).color(ChatColor.YELLOW);
							cms.sendMessage(output.create());
							
						}
					} catch (PlayerNeverConnectedException e) {
						cms.sendMessage(new TextComponent("This player has never played on this server!"));
					} catch (SQLException e) {
						cms.sendMessage(new TextComponent("An error occured, please contact an admin!"));
						e.printStackTrace();
					}
					
					break;
				}
			}
		});
	}
}
